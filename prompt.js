module.exports = {
  prompts: [
    {
      name: "i18n",
      message: "i18n - Internationalization",
      type: "select",
      choices: [
          { message: "None", name: "none" },
          { message: "react-i18next", name: "react-i18next" },
      ],
      default: "none",
    },
    {
      name: "example",
      message: "Add example resources?",
      type: "select",
      choices: [
          { message: "No", name: "none" },
          { message: "Yes", name: "example-resource" },
      ],
      default: "none",
    },
    {
      name: "docker",
      message: "Docker integration:",
      type: "select",
      pageSize: 3,
      choices: [
          { message: "None", name: "none" },
          { message: "Dockerfile", name: "docker" },
      ],
      default: "none",
  },
  ],
  ignores: [],
}