# The xxllnc Frontend Boilerplate cli created with Superplate

This is the source repository for [superplate](https://github.com/pankod/superplate)'s xxllnc cga source.

## About

Superplate lets you start rock-solid, production-ready React and Next.JS projects just in seconds. The command-line interface guides the user through setup and no additional build configurations are required.

This repository is created for the xxllnc cga frontend boilerplate source. With this source you can create a production ready React-admin application with xxllnc-react-components and Auth0 integratation.

### Quick Start

To use superplate, make sure you have npx is installed on your system (npx is shipped by default since npm 5.2.0).

To create a new app, run the following command:

```bash
npx superplate-cli@latest --source https://gitlab.com/xxllnc/zaakgericht/common-ground-apps/cga-cli.git <project_name>
```

You will be prompted with plugin options to create your project. The follwoing plugins are currently avaiable:

-   i18n internationalization
-   Docker integration
-   Example React-Admin resources


Once all questions are answered, superplate will install all plugins and dependencies. Then navigate to the project folder and launch it:

`npm run dev` or `yarn run dev`

> You can use npm or yarn depending on the choice you made during the creation of the project.

### License
Superplate is licensed under the MIT License, Copyright © 2021-present Pankod.

Visit the [superplate website](https://pankod.github.io/superplate/) for more information about Superplate.