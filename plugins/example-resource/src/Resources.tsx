import { Resource, ResourceProps } from 'react-admin'
import DemoRequests from './todo'
import About from './pages/about'

interface Permissions {
  scopes?: string[]
}

export interface CustomResourceProps extends ResourceProps {
  options: {
    label: string
    settings?: boolean
    scopes: string[]
    isSubMenu?: boolean
  }
}

const availableResources = (_permissions: Permissions): JSX.Element[] => [
  <Resource {...DemoRequests} />,
  <Resource {...About} />,
]

const fallback = (): JSX.Element[] => [
  <Resource
    name={DemoRequests.name}
    list={DemoRequests.list}
    icon={DemoRequests.icon}
    options={{
      label: 'todos'
    }}
  />
]

interface AResource {
  props: CustomResourceProps
}

export const resourcesForScopes = ({ scopes }: Permissions): JSX.Element[] => {

  if (!scopes || scopes.length === 0)
    return fallback()

  const resources = availableResources({ scopes })
    .filter((resource: AResource) => {
      const resourceWithScope = resource.props.options.scopes.find(scope => scopes.includes(scope))
      return resourceWithScope !== undefined
    })

  if (!resources || resources.length === 0)
    return fallback()

  return resources
}