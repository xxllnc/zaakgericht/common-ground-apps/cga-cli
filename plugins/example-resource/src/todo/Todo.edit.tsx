import { FC } from 'react'
import { Edit, SimpleForm, TextInput, ReferenceInput, EditProps, AutocompleteInput, BooleanInput } from 'react-admin'
import { HeaderTitle } from 'xxllnc-react-components'
import { ActionsToolbar } from '../components'

const EditActions: FC = () => (
  <ActionsToolbar source="todos" to="/todos"/>
)

export const TodoEdit: FC<Partial<EditProps>> = () => (
  <Edit
    title={<HeaderTitle title="Edit todo '%title%'" />}
    actions={<EditActions />}
  >
    <SimpleForm>
      <TextInput disabled source="id" />
      <ReferenceInput label="user" source="userId" reference="users">
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <TextInput label="description" source="title" />
      <BooleanInput label="completed" source="completed" />
    </SimpleForm>
  </Edit>
)