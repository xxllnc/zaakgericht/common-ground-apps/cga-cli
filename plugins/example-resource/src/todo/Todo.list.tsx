import { List, Datagrid, BooleanField, TextField, EditButton, ReferenceField } from 'react-admin'

export const TodoList = () => (
  <List title="Xxlnc boilerplate app - Todo's">
    <Datagrid>
      <TextField source="id" />
      <ReferenceField link={false} label="user" source="userId" reference="users">
        <TextField source="name" />
      </ReferenceField>
      <TextField label="description" source="title" />
      <BooleanField label="completed" source="completed" />
      <EditButton />
    </Datagrid>
  </List>
)