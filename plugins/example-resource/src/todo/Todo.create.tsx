import { FC } from 'react'
import { Create, SimpleForm, TextInput, ReferenceInput, AutocompleteInput, BooleanInput } from 'react-admin'

export const TodoCreate: FC = () => (
  <Create title="createTodo">
    <SimpleForm>
      <ReferenceInput label="user" source="userId" reference="users">
        <AutocompleteInput optionText="name" />
      </ReferenceInput>
      <TextInput label="description" source="title" />
      <BooleanInput label="completed" source="completed" />
    </SimpleForm>
  </Create>
)