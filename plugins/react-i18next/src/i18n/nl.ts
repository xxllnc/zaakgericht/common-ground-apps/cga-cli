import dutchMessages from 'ra-language-dutch'
import { nl } from 'xxllnc-react-components'

export const i18nDutch = {
  ...dutchMessages,
  ...nl,
  todos: 'Todo lijst',
  user: 'Gebruiker',
  description: 'Omschrijving',
  completed: 'Afgerond',
  createTodo: 'Maak een nieuw todo item',
}

