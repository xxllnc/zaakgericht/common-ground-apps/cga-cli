const base = {
  _app: {
      import: [
        'import { i18nDutch } from "./i18n"',
        'import polyglotI18nProvider from "ra-i18n-polyglot"'
      ],
      outer: [
        '// eslint-disable-next-line @typescript-eslint/ban-ts-comment',
        '//@ts-ignore',
        'const i18nProvider = polyglotI18nProvider(() => i18nDutch, "nl", { allowMissing: true })'
      ],
      inner: ['i18nProvider={i18nProvider}'],
      wrapper: [],
  },
};

module.exports = {
  extend() {
      return base;
  },
};