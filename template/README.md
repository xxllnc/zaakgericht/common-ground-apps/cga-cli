# Getting Started with <%= name %>

This project was bootstrapped with [xxllnc Frontend Boilerplate cli](https://gitlab.com/xxllnc/zaakgericht/common-ground-apps/cga-cli).

## Available Scripts

In the project directory, you can run:

### `<%= pmRun %> dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `<%= pmRun %> test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `<%= pmRun %> build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

The base for the xxllnc Frontend Boilerplate is Create React App, you can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
