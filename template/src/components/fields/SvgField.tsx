import get from 'lodash/get'
import { Typography, TableCellProps } from '@mui/material'
import { FieldProps, RaRecord, useRecordContext } from 'react-admin'
import PropTypes from 'prop-types'

type TextAlign = TableCellProps['align']
type SortOrder = 'ASC' | 'DESC'

const sanitizeFieldRestProps: (props: any) => any = ({
  addLabel,
  allowEmpty,
  basePath,
  cellClassName,
  className,
  emptyText,
  formClassName,
  fullWidth,
  headerClassName,
  label,
  link,
  linkType,
  locale,
  record,
  refetch,
  resource,
  sortable,
  sortBy,
  sortByOrder,
  source,
  textAlign,
  translateChoice,
  ...props
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
}) => props

const fieldPropTypes = {
  addLabel: PropTypes.bool,
  cellClassName: PropTypes.string,
  className: PropTypes.string,
  headerClassName: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  sortable: PropTypes.bool,
  sortBy: PropTypes.string,
  sortByOrder: PropTypes.oneOf<SortOrder>(['ASC', 'DESC']),
  source: PropTypes.string,
  textAlign: PropTypes.oneOf<TextAlign>([
    'inherit',
    'left',
    'center',
    'right',
    'justify',
  ]),
  emptyText: PropTypes.string,
}

export const SvgField = ({ source, className, emptyText, ...props }: SvgFieldProps): JSX.Element => {
  const record = useRecordContext<RaRecord>(props)
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call
  const image = typeof record === 'string' ? record : get(record, source) as void

  if (image !== null && image !== undefined) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const value = typeof image === 'string' ? image : get(image, source) as string
    const sanitizedImage = value && value.indexOf('blob:') === 0 ? value : `data:image/svg+xml;base64,${value}`
    return <img className={className} src={sanitizedImage} />
  }

  return (
    <Typography
      component="span"
      variant="body2"
      className={className}
      {...sanitizeFieldRestProps(props)}
    >
      {emptyText}
    </Typography>
  )
}

SvgField.defaultProps = {
  addLabel: true,
}

SvgField.propTypes = fieldPropTypes
SvgField.displayName = 'SvgField'

interface SvgFieldProps extends FieldProps {
  source: string,
}
