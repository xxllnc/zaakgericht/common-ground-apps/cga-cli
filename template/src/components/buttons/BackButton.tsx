import { ArrowBack } from '@mui/icons-material'
import { useEffect, useState } from 'react'
import { Button, ButtonProps } from 'react-admin'
import { Link } from 'react-router-dom'

interface BackButtonProps extends ButtonProps {
  label?: string,
  to?: string,
}

export const BackButton = (props: BackButtonProps): JSX.Element => {
  const { label, to } = props
  const [mountHistoryLength, setMountHistoryLength] = useState<number>(0)

  useEffect(() => setMountHistoryLength(window.history.length), [])
  const onClick = () => !to && window.history.go(mountHistoryLength - window.history.length - 1)

  return (
    <Button
      variant="text"
      onClick={onClick}
      label={label}
      component={ to ? Link : undefined }
      to={ to ? { pathname: '/' } : undefined }
      sx={theme => ({ marginRight: theme.spacing(1) })}
      {...props}
    >
      <ArrowBack />
    </Button>
  )
}
