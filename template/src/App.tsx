import { FC } from 'react'
import { Route, BrowserRouter } from 'react-router-dom'
import { getEnvVariable, Auth0ProviderWithHistory, theme, useAuth0User, CustomLayout  } from 'xxllnc-react-components'
import { Admin as ReactAdmin, CustomRoutes } from 'react-admin'
import AboutPage from './pages/about/About'
import LoginPage from './pages/login/Login'
import providers from './providers'
import { resourcesForScopes } from './Resources'
<%- _app.import.join("\n") _%>

export const redirectUri = `${window.location.origin}${process.env.PUBLIC_URL}/login`
const scope = 'cga.user cga.admin openid profile'
const getDomain = () => getEnvVariable('REACT_APP_AUTH0_DOMAIN')
const getClientID = () => getEnvVariable('REACT_APP_AUTH0_CLIENT_ID')
const getAudience = () => getEnvVariable('REACT_APP_AUTH0_AUDIENCE')

const returnTo = `${window.location.origin}${process.env.PUBLIC_URL}/login`
const mock = getEnvVariable('REACT_APP_MOCK_REQUESTS').toUpperCase() === 'TRUE'

<%- _app.outer?.join("\n") %>

const Admin: FC = () => {
  const auth0 = useAuth0User()

  return (
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  <ReactAdmin
    <%- _app.inner.join("\n") %>
    authProvider={providers.auth(auth0, returnTo, mock)}
    dataProvider={providers.data}
    loginPage={LoginPage}
    layout={CustomLayout}
    disableTelemetry
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    theme={theme}
  >
    { resourcesForScopes }
    <CustomRoutes>
      <Route path="/about" element={<AboutPage />} />
    </CustomRoutes>
  </ReactAdmin>)
}

const App: FC = () => (
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Auth0ProviderWithHistory
      audience={getAudience()}
      scope={scope}
      redirectUri={redirectUri}
      domain={getDomain()}
      clientId={getClientID()}
    >
      <Admin />
    </Auth0ProviderWithHistory>
  </BrowserRouter>
)

export default App
