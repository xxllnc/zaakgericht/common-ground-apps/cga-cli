import PlaylistAddCheckIcon from '@mui/icons-material/PlaylistAddCheck'
import { CustomResourceProps } from '../Resources'
import { TodoList } from './Todo.list'
import { TodoEdit } from './Todo.edit'
import { TodoCreate } from './Todo.create'

const DemoRequests: CustomResourceProps = {
  name: 'todos',
  list: TodoList,
  create: TodoCreate,
  edit: TodoEdit,
  icon: PlaylistAddCheckIcon,
  options: {
    label: 'todos',
    scopes: ['cga.admin', 'cga.user']
  }
}

export default DemoRequests
