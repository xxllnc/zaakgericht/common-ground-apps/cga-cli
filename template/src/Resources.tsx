import { ResourceProps } from 'react-admin'

interface Permissions {
  scopes?: string[]
}

export interface CustomResourceProps extends ResourceProps {
  options: {
    label: string
    settings?: boolean
    scopes: string[]
    isSubMenu?: boolean
  }
}

const availableResources = (_permissions: Permissions): JSX.Element[] => []

const fallback = (): JSX.Element[] => []

interface AResource {
  props: CustomResourceProps
}

export const resourcesForScopes = ({ scopes }: Permissions): JSX.Element[] => {

  if (!scopes || scopes.length === 0)
    return fallback()

  const resources = availableResources({ scopes })
    .filter((resource: AResource) => {
      const resourceWithScope = resource.props.options.scopes.find(scope => scopes.includes(scope))
      return resourceWithScope !== undefined
    })

  if (!resources || resources.length === 0)
    return fallback()

  return resources
}