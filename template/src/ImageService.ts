import logo from './assets/images/logo.jpg'
import background from './assets/images/xxllnc-uitgesproken-eenvoudig.svg'

export const ImageService = {
  logo,
  background
}