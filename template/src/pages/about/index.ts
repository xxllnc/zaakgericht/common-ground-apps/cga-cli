import InfoIcon from '@mui/icons-material/Info'
import { CustomResourceProps } from '../../Resources'

const About: CustomResourceProps = {
  name: 'about',
  icon: InfoIcon,
  options: {
    label: 'Over',
    settings: true,
    scopes: ['cga.admin', 'cga.user']
  }
}

export default About
