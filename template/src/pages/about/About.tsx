import { Title } from 'react-admin'
import { LicensesInformation } from 'xxllnc-react-components'
import licenses from '../../licenses.json'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'

const About = (): JSX.Element => (
  <Card>
    <Title title="Over" />
    <CardContent>
      <LicensesInformation licenses={licenses} />
    </CardContent>
  </Card>
)

export default About