import jsonServerProvider from 'ra-data-json-server'
import { authProvider, getEnvVariable } from 'xxllnc-react-components'

const getUrl = () => getEnvVariable('REACT_APP_SERVER_URL')

const providers = {
  auth: authProvider,
  data: jsonServerProvider(getUrl()),
  url: getUrl()
}

export default providers
